// page init
jQuery(function(){
    jQuery(document).on("click", ".sidebar .main-nav-item .list-wrap", function(e) {
        jQuery(this).parent('.main-nav-item').toggleClass('closed');
    });
    jQuery(document).on("click", "#mobile-navigation .arrow", function(e) {
        jQuery(this).toggleClass('collapsed');
    });
});