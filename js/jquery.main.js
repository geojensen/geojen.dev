// page init
jQuery(function(){
    TopScroll();
    $(window).scroll(function() {
        TopScroll();
    });

    $(document).on("click", ".back-to-top", function(e) {
        e.preventDefault();
        $('body, html').animate({scrollTop: 0}, 1200);
    });

    $(document).on("click", ".video-links a", function(e) {
        e.preventDefault();
        var id  = $(this).attr('href');
        var top = $(id).offset().top;
        $('body, html').animate({scrollTop: top}, 1200);
    });
});

function TopScroll(){
    if($(document).scrollTop() > 500) {
        jQuery('.back-to-top').addClass('show');
    } else {
        jQuery('.back-to-top').removeClass('show');
    }
}